myApp.run(function ($rootScope, Facebook) {
    $rootScope.Facebook = Facebook;
})

myApp.factory('Facebook', function () {

    var self = this;
    this.auth = null;

    return {
        getAuth: function () {
            return self.auth;
        },
        login: function () {
            FB.login(function (response) {
                if (response.authResponse) {
                    self.auth = response.authResponse;
                    console.log(response.authResponse.userID);
                } else {
                    console.log('Facebook login failed', response);
                }
            })

        },
        logout: function () {
            FB.logout(function (response) {
                if (response) {
                    self.auth = null;
                } else {
                    console.log('Facebook logout failed.', response);
                }
            })
        }
    }
})


window.fbAsyncInit = function () {
    FB.init({
        appId: '132740420269017'
    });
};

// Load the SDK Asynchronously
(function (d) {
    var js, id = 'facebook-jssdk',
        ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));
