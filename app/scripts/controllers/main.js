'use strict';

myApp.controller('MainCtrl', function ($state, $scope, persons, $rootScope) {

    $rootScope.$state = $state;

    $scope.currentState = $state.current;

    $scope.messages = [
        {id: 0, message: 'Welcome message'}
    ];
    $scope.onClickShare = function () {
        toastr.success('Gracias por recomendar PIT!', 'People I Trust')
    }
});
 
