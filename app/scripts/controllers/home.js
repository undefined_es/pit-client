'use strict';

myApp.factory("persons", function() {
  var persons = [ {name:'John', surname:'Doe'}, {name:'Steve', surname:'Jobs'}];

  return persons;
})

myApp.controller('HomeCtrl', function ($scope, persons) {
    $scope.persons = persons;
  });
  
  
