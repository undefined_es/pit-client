'use strict';


var myApp = angular.module('pitApp', ['ui.state', 'angular-carousel', 'ui.bootstrap'])
  .config(function($stateProvider, $urlRouterProvider){
	  
    $urlRouterProvider.otherwise('/');

    $stateProvider	
      .state('main', {
            url: '/',
            templateUrl: 'views/main.html'
        })

      .state('welcome', {
            url: '/welcome',
            templateUrl: 'views/welcome.html'
      })
	  
      .state('home', {
		  url: '/home',
          controller:'HomeCtrl',
		  templateUrl: 'views/home.html'
		})

      .state('profile', {
            url: '/profile',
            controller:'ProfileCtrl',
            templateUrl: 'views/profile.html'
      })

      .state('invite', {
            url: '/invite',
            controller:'InviteCtrl',
            templateUrl: 'views/invite.html'
        })

      .state('messages', {
		  url: '/messages',
  		  templateUrl: 'views/messages.html'
  		})
		
      .state('help', {
 		  url: '/help',
   		  templateUrl: 'views/help.html'
   		})
		
      .state('addFriends', {
 		  url: '/add_friends',
   		  templateUrl: 'views/add_friends.html'
   		})
		
      .state('config', {
 		  url: '/config',
   		  templateUrl: 'views/config.html'
        })
})

